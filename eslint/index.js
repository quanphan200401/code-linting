module.exports.rules = {
  'no-character-variable': (context) => ({
    Identifier(node) {
      if (node.name.length <= 1) {
        context.report(node, 'Variable name should not a single character');
      }
    },
  }),
};
